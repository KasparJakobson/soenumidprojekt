<?php
/**
 * Api to message somebody
 * @version 1.0.0
 */

require_once __DIR__ . '/vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new Slim\App($c);

/**
 * POST addMessage
 * Summary: Add a new message
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->POST('/message', function ($request, $response, $args) {

    require('sql.php');
    require('checkLogin.php');


    if (checkToken()) {
        $query = "INSERT INTO message (author_id, receiver_id, message_text) VALUES (?,?,?)";

        $stmt = $conn->prepare($query);

        $author_id = $request->getParsedBody()['author_id'];
        $receiver_id = $request->getParsedBody()['receiver_id'];
        $message_text = $request->getParsedBody()['message_text'];

        $stmt->bind_param("iis", $author_id, $receiver_id, $message_text);
        $stmt->execute();

        return $response;
    } else {
        header("HTTP/1.1 401 Unauthorized");
        return $response;
    }
});


/**
 * DELETE deleteMessage
 * Summary: Deletes a message
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->DELETE('/message/{message_id}', function ($request, $response, $args) {

    require_once('sql.php');
    require_once('checkLogin.php');

    if (checkToken()) {
        $get_id = $request->getAttribute('message_id');

        $query = "DELETE from message WHERE message_id = $get_id";

        $result = $conn->query($query);

        return $response;
    } else {
        header("HTTP/1.1 401 Unauthorized");
        return $response;
    }
});


/**
 * GET getMessageById
 * Summary: Find message by ID
 * Notes: Returns a single message
 * Output-Formats: [application/xml, application/json]
 */
$app->GET('/message', function ($request, $response, $args) {
    require_once('sql.php');

    $query = "select * from message order by message_id";
    $result = $conn->query($query);

    // var_dump($result);

    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }


//    $response->write('How about implementing getMessageById as a GET method ?');
    return json_encode($data);
});


/**
 * PUT updateMessage
 * Summary: Update an existing message
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->PUT('/message/{message_id}', function ($request, $response, $args) {

    require_once('sql.php');
    require_once('checkLogin.php');


    if (checkToken()) {

        $get_id = $request->getAttribute('message_id');

        $query = "UPDATE message SET author_id = ?, receiver_id = ?, message_text = ? WHERE message_id = $get_id";

        $stmt = $conn->prepare($query);

        $author_id = $request->getParsedBody()['author_id'];
        $receiver_id = $request->getParsedBody()['receiver_id'];
        $message_text = $request->getParsedBody()['message_text'];

        $stmt->bind_param("iis", $author_id, $receiver_id, $message_text);

        $stmt->execute();
        return $response;
    } else {
        header("HTTP/1.1 401 Unauthorized");
        return $response;
    }

    return $response;
});

$app->POST('/login', function ($request, $response, $args) {

    $login_username = addslashes($request->getParsedBody()["username"]);
    $login_password = addslashes($request->getParsedBody()["password"]);

    require_once('sql.php');

    $query = "SELECT * FROM users WHERE username='$login_username' && password='$login_password'";
    $result = $conn->query($query);

    $data = [];

    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    if ($data ) {
        function generateRandomString($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $query = "INSERT INTO tokens (token_value) VALUES (?)";

        $stmt = $conn->prepare($query);

        $token_value = generateRandomString();

        $stmt->bind_param("s", $token_value);
        $stmt->execute();

        setcookie("token", $token_value);
        return $response;
    } else {
        header("HTTP/1.1 401 Unauthorized");
        return $response;
    }
});


$app->run();