var app = new Vue({
    el: '#app',
    data: {
        loginModal: {
            username: null,
            password: null
        },

        allMessages: [],

        postMessage: {
            author_id: null,
            receiver_id: null,
            message_text: null
        },

        updateMessage: {
            message_id: null,
            author_id: null,
            receiver_id: null,
            message_text: null
        },

        deleteMessage: {
            message_id: null,
        }
    },
    mounted: function () {
        this.get_all_messages()
    },
    methods: {
        login() {
            this.$http.post('http://localhost/sonumid/index.php/login', this.loginModal)
                .then(function (response) {
                    console.log(response);
                    this.postMessage.token_value = response.body;
                    this.updateMessage.token_value = response.body;
                    this.deleteMessage.token_value = response.body;
                });
        },

        get_all_messages() {
            this.$http.get('http://localhost/sonumid/index.php/message')
                .then(function (response) {
                    this.allMessages = response.body;
                });
        },

        post_message() {
            this.$http.post('http://localhost/sonumid/index.php/message', this.postMessage)
                .then(function (response) {
                    console.log(response);
                });
        },

        update_message() {
            this.$http.put('http://localhost/sonumid/index.php/message/' + this.updateMessage.message_id, this.updateMessage)
                .then(function (response) {
                    console.log(response);
                });
        },

        delete_message() {
            this.$http.delete('http://localhost/sonumid/index.php/message/' + this.deleteMessage.message_id)
                .then(function (response) {
                    console.log(response);
                });
        }

    },
});