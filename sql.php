<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 03.10.18
 * Time: 11:14
 */

$servername = "localhost";
$username = "root";
$password = "";
$database="messages";

// Create connection
global $conn;
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

